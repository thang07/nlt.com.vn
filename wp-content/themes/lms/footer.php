<?php
/**
 * Created by PhpStorm.
 * User: thangnm
 * Date: 3/20/15
 * Time: 3:48 PM
 */
?>
<?php// include ('partners.php') ?>
<footer>
    <div class="container">
        <div class="row">
            <div class="footers text-center">
                <div class="address">
                  <?php  echo apply_filters('the_content', get_post_field('post_content', 142));?>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<?php wp_footer(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/carouselengine/amazingcarousel.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/carouselengine/initcarousel-1.js"></script>
<script >
    $(function(){
        // this will get the full URL at the address bar
        var url = window.location.href;

        // passes on every "a" tag
        $("#round-menus-header a").each(function() {
            // checks if its the same on the address bar
            if(url == (this.href)) {
                $(this).closest(".current_menu_custom a").addClass("active");
            }
        });
    });
</script>
</body>
</html>