<?php
/**
 * Created by PhpStorm.
 * User: thangnm
 * Date: 3/20/15
 * Time: 3:52 PM
 */

if (!function_exists('lms_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function lms_setup()
    {

        /**
         * Add default posts and comments RSS feed links to head.
         */
        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'lnv'),
            'secondary' => __('Secondary Menu', 'lnv'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
        ));

//        add_theme_support('post-formats', array(
//            'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat', 'youtube'
//        ));
//

        // Add support for the Site Logo plugin and the site logo functionality in JetPack
        // https://github.com/automattic/site-logo
        // http://jetpack.me/
        add_theme_support('site-logo', array('size' => 'full'));

        // Declare support for title theme feature
        add_theme_support('title-tag');
    }
endif; // storefront_setup
add_action('after_setup_theme', 'lms_setup');


function lnv_scripts()
{
    wp_enqueue_style('bootstrap-style', get_template_directory_uri() . '/css/bootstrap.css', '', '3.3.2');

    wp_enqueue_style('style', get_stylesheet_uri(), '', null);

    wp_enqueue_script('bootstrap-script', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), null, true);

//    wp_enqueue_script('common-script', get_template_directory_uri() . '/js/script.js', array('jquery'), null, true);

}

add_action('wp_enqueue_scripts', 'lnv_scripts', 1);

//add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);
//
//function special_nav_class($classes, $item)
//{
//    if (in_array('current-menu-item', $classes) ) {
//        $classes[] = 'active';
//    }
//    return $classes;
//}

add_filter('nav_menu_css_class', 'current_type_nav_class', 10, 2);
function current_type_nav_class($classes, $item) {
    // Get post_type for this post
    $post_type = get_query_var('post_type');

    // Go to Menus and add a menu class named: {custom-post-type}-menu-item
    // This adds a 'current_page_parent' class to the parent menu item
    if( in_array( $post_type.'menu-item-type-custom', $classes ) )
        array_push($classes, 'current_menu_custom');

    return $classes;
}

add_action( 'widgets_init', 'theme_slug_widgets_init' );
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'theme-slug' ),
        'id' => 'sidebar-1',
        'description' => __( 'Widgets ', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}

//BEGIN thiet lap so luong truy cap bai viet
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//BEGIN lay so luong truy cap bai viet
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count;
}

// Remove issues with prefetching adding extra views
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
//end
add_theme_support( 'title-tag' );
