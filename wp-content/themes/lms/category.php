<?php

get_header() ?>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.quick.pagination.min.js"
            type="text/javascript">
    </script>

    <script type="text/javascript">
        var conlic = jQuery.noConflict();
        conlic(document).ready(function ($) {
            conlic("ul.pagination1").quickPagination({
                pageSize: "10",
                currentPage: 1,
                holder: null,
                pagerLocation: "after"
            });
        });
    </script>
    <div class="wrapper-product">
        <div class="container">
            <div class="row list-product">
                <div class="">
                    <div class="col-md-8">
                        <div class="home text-uppercase">
                            <i class="glyphicon glyphicon-home "></i> <?php _e('Home'); ?></a> &raquo;
                            <span class="text-uppercase">  <?php echo single_cat_title('', false); ?></span>
                        </div>
                        <ul class="pagination1 list">
                            <?php while (have_posts()) : the_post(); ?>
                                <li class="media item">
                                    <div class="media-left">
                                        <div class="img-border">
                                            <a href="<?php the_permalink(); ?>">
                                                <?php echo the_post_thumbnail(array(150, 150)); ?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading"><a
                                                href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

                                        <p class="short-detail">
                                            <?php echo wp_trim_words(get_the_excerpt(), 50); ?>
                                        </p>
                                    </div>
                                </li>
                            <?php endwhile; ?>
                        </ul>

                    </div>
                    <div class="col-md-4 ">
                        <?php include('products.php') ?>
                        <?php include('mobile.php') ?>
                        <?php include('services.php') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer() ?>