<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 4/8/2015
 * Time: 3:13 PM
 */

get_header() ?>

    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.quick.pagination.min.js"
            type="text/javascript">
    </script>

    <script type="text/javascript">
        var conlic=jQuery.noConflict();
        conlic(document).ready(function ($) {
            conlic("ul.pagination1").quickPagination({pageSize: "11", currentPage: 1, holder: null, pagerLocation: "after"});
        });
    </script>
    <div class="wrapper-product">
        <div class="container">
            <div class="row list-product">
                <div class="col-md-12 ">
                    <div class="col-md-8">
                        <div class="home text-uppercase">
                            <i class="glyphicon glyphicon-home "></i><a href="<?php bloginfo('home'); ?>"> <?php _e('Home'); ?></a> &raquo;
                            <span class="text-uppercase">  <?php echo the_title() ?></span>
                        </div>
                        <ul class="list services pagination1">
                            <?php
                            $args = array('category_name' => 'dich-vu');
                            $lastposts = get_posts($args);
                            foreach ($lastposts as $post) :
                                setup_postdata($post); ?>
                                <li> <i class="glyphicon glyphicon-chevron-right"></i>
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </li>
                            <?php endforeach;
                            wp_reset_postdata(); ?>
                        </ul>

                    </div>
                    <div class="col-md-4 ">
                        <div class="list">
                            <?php include('products.php') ?>
                            <?php include('mobile.php') ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer() ?>