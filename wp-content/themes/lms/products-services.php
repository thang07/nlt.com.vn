<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 3/31/2015
 * Time: 4:41 PM
 */ ?>
<div class="product-service">
    <div class="container">
        <div class="row">
            <div class=" col-sm-12 col-md-12">
                <div class="col-sm-4 col-md-4 item">
                    <div class="item text-center">
                        <img src="<?php echo get_template_directory_uri() ?>/images/box.png" height="108px"/>

                        <h2> <a href="<?php echo get_page_link(132);?>"><?php echo get_cat_name( 2 ) ?> </a> </h2>
                    </div>
                    <div class="list-item">
                        <?php
                        $args = array('category_name' => 'san-pham', 'order' => 'ASC', 'numberposts' => '2');
                        $lastposts = get_posts($args);
                        foreach ($lastposts as $post) :
                            setup_postdata($post); ?>
                            <div class="media">
                                <div class="media-left">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php echo the_post_thumbnail(array(85,84)); ?>
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading"><a
                                            href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

                                    <p class="short-detail">
                                        <?php echo wp_trim_words(get_the_excerpt(), 10); ?>
                                    </p>

                                </div>
                            </div>
                        <?php endforeach;
                        wp_reset_postdata(); ?>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 item">
                    <div class="item text-center">
                        <img src="<?php echo get_template_directory_uri() ?>/images/laptop.png" height="108px"/>

                        <h2><a href="<?php echo get_category_link(9);?>"><?php echo get_cat_name(9) ?></a></h2>
                    </div>
                    <div class="list-item">
                        <?php
                        $args = array('category_name' => 'phan-mem-moblie', 'numberposts' => '2');
                        $lastposts = get_posts($args);
                        foreach ($lastposts as $post) :
                            setup_postdata($post); ?>
                            <div class="media">
                                <div class="media-left">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php echo the_post_thumbnail(array(85,84)); ?>
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading"><a
                                            href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

                                    <p class="short-detail">
                                        <?php echo wp_trim_words(get_the_excerpt(), 10); ?>
                                    </p>

                                </div>
                            </div>
                        <?php endforeach;
                        wp_reset_postdata(); ?>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 item">
                    <div class="item text-center">
                        <img src="<?php echo get_template_directory_uri() ?>/images/mouse.png" height="108px"/>

                        <h2><a href="<?php echo get_page_link(139);?>"> <?php echo get_cat_name( 3 ) ?></a></h2>
                    </div>
                    <ul class="list-item">
                        <?php
                        $args = array('category_name' => 'dich-vu');
                        $lastposts = get_posts($args);
                        foreach ($lastposts as $post) :
                            setup_postdata($post); ?>
                            <li><i class="glyphicon glyphicon-chevron-right"></i>  &nbsp; <a href="<?php echo the_permalink()?>"><?php the_title()?></a></li>
                        <?php endforeach;
                        wp_reset_postdata(); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
