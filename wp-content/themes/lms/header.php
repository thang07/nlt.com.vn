<!doctype html>
<!--[if IE 7]>
<html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8]>
<html class="no-js ie8" lang="en"> <![endif]-->
<!--[if IE 9]>
<html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <meta http-equiv="content-language" content="vi"/>
    <meta name="description"
          content="lms,lms.vn, mraovat, vimeeting, vimeeting.vn,mraovat.vn,tu dien 3000 tu, 3000 tu dien,
           mua ban tren di dong, app di dong"/>
    <meta name="keywords"
          content="lms,lms.vn, mraovat, vimeeting, vimeeting.vn,mraovat.vn,tu dien 3000 tu, 3000 tu dien,
           mua ban tren di dong, app di dong"/>
    <meta name="author" content="lms.vn" about="Website th??ng hi?u v� b?n quy?n thu?c lms.vn"/>
    <meta name="author" content="hongdiepbach@gmail.com" about="Coding website"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" type="image/x-icon"/>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <!--[if lte IE 8]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:700,400' rel='stylesheet' type='text/css'>
    <!--[if IE ]>
    <link type="text/css" media="screen,projection" rel="stylesheet" href="css/ie.css"/>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="js/html5.js"></script>
    <![endif]-->
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
<div id="wrapper">
    <!-- start header -->
    <header>
        <div class="navbar navbar-default navbar-static-top navbar-fixed-top menu-header">
            <div class="container">
                <div class="languages text-right">
                    <?php qtrans_generateLanguageSelectCode('image'); ?>
                </div>
                <div class="row row-head">
                    <div class="navbar-header">
                        <a class="navbar-brand" href=" <?php echo esc_url(home_url('/')); ?> ">
                            <h1 class="site-title">
                                <img alt="Brand" src="<?php echo get_template_directory_uri() ?>/images/logo.jpg" height="54px" width="px"
                                     class="img-responsive logan" alt="logan"/>
                            </h1>

                            <h2 class="site-title" style="display: none"
                                title="<?php echo $blog_title = get_bloginfo('name'); ?>"><?php echo $blog_title = get_bloginfo('name'); ?></h2>
                        </a>
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="navbar-collapse collapse " id="round-menus-header">
                        <?php
                        $defaults = array(
                            'container' => false,
                            'menu_class' => 'nav navbar-nav',
                            'echo' => true,
                        );
                        wp_nav_menu($defaults);
                        ?>

                        <form action="<?php echo home_url( '/' ) ?>" id="searchform" method="get"
                              class="navbar-form navbar-left" role="search">

                            <div class="form-group">
                                <input type="text" id="s" name="s" value="" size="20" class="form-control"/>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--end-->

