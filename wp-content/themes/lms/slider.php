<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/flexslider.css" type="text/css"
      media="screen"/>
<section class="slider">
    <div class="container">
        <div class="row">
            <div class="flexslider">
                <ul class="slides">
                    <?php
                    $args = array('category_name' => 'slides', 'numberposts' => '10');
                    $lastposts = get_posts($args);
                    foreach ($lastposts as $post) :
                        setup_postdata($post); ?>
                        <li>
                            <?php echo the_post_thumbnail('post-thumbnail', array('class' => "media-object img-responsive")); ?>

                        </li>
                    <?php endforeach;
                    wp_reset_postdata(); ?>
                </ul>
            </div>
        </div>
    </div>
</section>


<!-- FlexSlider -->
<script defer src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider.js"></script>

<script type="text/javascript">

    jQuery(window).load(function () {
        jQuery('.flexslider').flexslider({
            animation: "slide",
            start: function (slider) {
                jQuery('body').removeClass('loading');
            }
        });
    });
</script>
