<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 4/8/2015
 * Time: 2:06 PM
 */
get_header() ?>
<div class="content">
    <div class="customers">
        <div class="container">
            <div class="home text-uppercase">
                <i class="glyphicon glyphicon-home"></i> <?php _e('Home'); ?></a> &raquo;
                <span class="text-uppercase">  <?php echo the_title() ?></span>
            </div>
            <h3 class="text-uppercase"> <?php echo the_title() ?></h3>

            <div class="row">
                <?php
                $i = 1;
                $args = array('category_name' => 'khach-hang', 'order' => 'ASC', 'numberposts' => '12');
                $lastposts = get_posts($args);
                foreach ($lastposts as $post) :
                    setup_postdata($post); ?>
                    <div class="col-sm-2 item <?php if ($i % 6 == 0) {
                        echo 'last';
                    }else if ($i % 6 == 1)
                    {
                        echo 'first';
                    }else ?>">
                        <a href="<?php if(get_field('url-links')){echo get_field('url-links');}?>" target="_blank" title="<?php echo the_title() ?>">
                        <?php echo the_post_thumbnail('post-thumbnail', array( 'class' => "media-object img-responsive")); ?>
                        </a>
                    </div>
                    <?php $i++; ?>
                <?php endforeach;
                wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
    <?php include('products-services.php') ?>
</div>
<?php get_footer() ?>
