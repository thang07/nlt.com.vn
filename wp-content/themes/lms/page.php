<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 4/9/2015
 * Time: 9:31 AM
 */
get_header() ?>
    <div class="wrapper-product">
        <div class="container">
            <div class="row list-product">
                <div class="col-md-12 ">
                    <div class="col-md-8">
                        <div class="home text-uppercase">
                            <i class="glyphicon glyphicon-home "></i><a href="<?php bloginfo('home'); ?>"> <?php _e('Home'); ?></a> &raquo;
                            <span class="text-uppercase">  <?php echo the_title() ?></span>
                        </div>

                        <div class="details list">
                            <?php
                            while (have_posts()) : the_post();
                                ?>
                                <div class="date-post text-left">
                                    <?php setPostViews(get_the_ID());?>
                                    <i> - Viewer: <?php echo getPostViews(get_the_ID());?></i>
                                    <i>- Posted date: <?php echo get_the_date('d/m/Y'); ?></span></i>
                                </div>
                                <?php the_content()?>
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <?php include('products.php') ?>
                        <?php include('mobile.php') ?>
                        <?php include('services.php') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer() ?>