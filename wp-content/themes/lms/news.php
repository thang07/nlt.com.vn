<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 4/7/2015
 * Time: 3:01 PM
 */
?>

<div class="news">
    <div class="container">
        <div class="row">
            <div class="media">
                <div class="media-left">
                    <?php $url = get_permalink();
                    if (strpos($url, '/en/') !== false) {
                        ?>
                        <img src="<?php echo get_template_directory_uri() ?>/images/newen.png" class="img-responsive"
                             height="61px" width="89px"/>

                    <?php } else { ?>
                        <img src="<?php echo get_template_directory_uri() ?>/images/new.png" class="img-responsive"
                             height="61px" width="89px"/>

                    <?php } ?>
                </div>
                <div class="media-body">
                    <marquee behavior="scroll" scrollamount="3" direction="left" width="99%">
                        <?php
                        $args = array('category_name' => 'tin-tuc', 'numberposts' => '6', 'orderby' => 'rand');
                        $lastposts = get_posts($args);
                        foreach ($lastposts as $post) :
                            setup_postdata($post); ?>
                            <h5 class="media-heading"> <i class="glyphicon glyphicon-chevron-right"></i>
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                            </h5>
                        <?php endforeach;
                        wp_reset_postdata(); ?>
                    </marquee>
                </div>
            </div>
        </div>
    </div>
</div>