<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 4/8/2015
 * Time: 1:31 PM
 */
get_header()?>

    <div class="content wrapper-contact">
        <div class="container">
            <div class="row">
                <div class="form-contact">

                    <?php
                        $url = get_permalink();

                        if(strpos($url,'/en/')!==false){?>
                             <h3 class="fonts-h">Contact form </h3>
                             <?php echo do_shortcode('[contact-form-7 id="129" title="Untitled"]'); ?>
                       <?php }else{?>
                            <h3 class="fonts-h">Liên hệ</h3>
                            <?php echo do_shortcode('[contact-form-7 id="128" title="Contact form 1"]'); ?>

                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
<?php get_footer()?>