<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 4/8/2015
 * Time: 3:13 PM
 */

get_header() ?>

    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.quick.pagination.min.js"
            type="text/javascript">
    </script>

    <script type="text/javascript">
        var conlic = jQuery.noConflict();
        conlic(document).ready(function ($) {
            conlic("ul.pagination1").quickPagination({
                pageSize: "10",
                currentPage: 1,
                holder: null,
                pagerLocation: "after"
            });
        });
    </script>
    <div class="wrapper-product">
        <div class="container">
            <div class="row list-product">
                <div class="">
                    <div class="col-md-8">
                        <div class="home text-uppercase">
                            <i class="glyphicon glyphicon-home "></i><a
                                href="<?php bloginfo('home'); ?>"> <?php _e('Home'); ?></a> &raquo;
                            <span class="text-uppercase">  <?php echo the_title() ?></span>
                        </div>
                        <ul class="pagination1 list">
                            <?php
                            $args = array('category_name' => 'san-pham');
                            $lastposts = get_posts($args);
                            foreach ($lastposts as $post) :
                                setup_postdata($post); ?>
                                <li class="media item">
                                    <div class="media-left">
                                        <div class="img-border">
                                            <a href="<?php the_permalink(); ?>">
                                                <?php echo the_post_thumbnail(array(150, 150)); ?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading"><a
                                                href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

                                        <p class="short-detail">
                                            <?php echo wp_trim_words(get_the_excerpt(), 50); ?>
                                        </p>
                                    </div>
                                </li>
                            <?php endforeach;
                            wp_reset_postdata(); ?>
                        </ul>
                    </div>
                    <div class="col-md-4 ">
                        <div class="list">
                            <?php include('mobile.php') ?>
                            <?php include('services.php') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer() ?>