<?php get_header();?>
    <div class="wrapper-product">
        <div class="container">
            <div class="row list-product">
                <div class="col-md-12 ">
                    <div class="col-md-8">
                        <div class="home text-uppercase">
                            <i class="glyphicon glyphicon-home text-uppercase"></i> <a href="<?php bloginfo('home'); ?>"> <?php _e('Home'); ?></a> &raquo;
                            <span class="text-uppercase">   <?php echo the_title() ?></span>
                        </div>

                        <?php
                        while (have_posts()) : the_post();
                            ?>
                            <?php setPostViews(get_the_ID()); ?>

                            <div class="date-post text-left">
                                <i> - Viewer: <?php echo getPostViews(get_the_ID());?></i>
                                <i> - Posted date: <?php echo get_the_date('d/m/Y'); ?></span></i>
                            </div>
                            <div class="details list">
                                <?php the_content() ?>
                            </div>
                            <div class="text-right">
                                <div class="fb-like " data-href="<?php echo the_permalink() ?>"
                                     data-layout="button_count" data-action="like" data-show-faces="false"
                                     data-share="true"></div>
                                <div id="fb-root"></div>
                                <script>(function (d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) return;
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.3&appId=1082736728409722";
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }(document, 'script', 'facebook-jssdk'));</script>
                            </div>
                            <h5 class="text-uppercase"> <?php
                            $url = get_permalink();
                            if(strpos($url,'/en/')!==false){
                                    echo "RELATED POSTS: ";
                                }else{ echo "BÀI VIẾT LIÊN QUAN:";} ?></h5>
                            <ul class="related-post">
                                <?php
                                    $current_post = $post->ID;
                                    $categories = get_the_category();
                                    foreach ($categories as $category) :
                                        $posts = get_posts('showposts=4&offset=0&orderby=rand&category=' . $category->term_id . '&exclude=' . $current_post);
                                        foreach ($posts as $post) :
                                            ?>
                                            <li> <i class="glyphicon glyphicon-chevron-right"></i>  &nbsp;  <a href="<?php the_permalink(); ?>" title="Xem"><?php the_title(); ?></a></li>
                                        <?php endforeach;
                                 endforeach; ?>
                            </ul>
                        <?php endwhile; ?>
                    </div>
                    <div class="col-md-4 ">
                        <?php include('products.php') ?>
                        <?php include('mobile.php') ?>
                        <?php include('services.php') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer() ?>