<?php get_header(); ?>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.quick.pagination.min.js"
            type="text/javascript">
    </script>

    <script type="text/javascript">
        var conlic = jQuery.noConflict();
        conlic(document).ready(function ($) {
            conlic("ul.pagination1").quickPagination({
                pageSize: "10",
                currentPage: 1,
                holder: null,
                pagerLocation: "after"
            });
        });
    </script>
    <div class="wrapper-product">
        <div class="container">
            <div class="row list-product">
                <div class="col-md-12 ">
                    <div class="col-md-8">
                        <div class="home text-uppercase">
                            <i class="glyphicon glyphicon-home text-uppercase"></i> <a
                                href="<?php bloginfo('home'); ?>"> <?php _e('Home'); ?></a> &raquo;
                            <span class="text-uppercase">   <?php echo the_title() ?></span>
                        </div>
                        <ul class="pagination1 list">
                            <?php if (have_posts()) : ?>
                                <?php  while (have_posts()) : the_post();   ?>
                                    <li class="media item">
                                        <div class="media-left">
                                            <div class="img-border">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php echo the_post_thumbnail(array(150, 150)); ?>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a
                                                    href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

                                            <p class="short-detail">
                                                <?php echo wp_trim_words(get_the_excerpt(), 50); ?>
                                            </p>
                                        </div>
                                    </li>

                                <?php endwhile;
                            endif; ?>
                        </ul>
                        <div class="text-right">
                            <div class="fb-like " data-href="<?php echo the_permalink() ?>"
                                 data-layout="button_count" data-action="like" data-show-faces="false"
                                 data-share="true"></div>
                            <div id="fb-root"></div>
                            <script>(function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s);
                                    js.id = id;
                                    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.3&appId=1082736728409722";
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <?php include('products.php') ?>
                        <?php include('mobile.php') ?>
                        <?php include('services.php') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer() ?>