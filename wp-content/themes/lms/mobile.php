<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 4/8/2015
 * Time: 3:15 PM
 */?>
<div class="moblie">
    <h3 class="text-uppercase"><?php echo get_cat_name(9) ?></h3>
    <?php
    $args = array('category_name' => 'phan-mem-moblie', 'numberposts' => '2');
    $lastposts = get_posts($args);
    foreach ($lastposts as $post) :
    setup_postdata($post); ?>
    <div class="media">
        <div class="media-left">
            <a href="<?php the_permalink(); ?>">
                <?php echo the_post_thumbnail( array(85,85) ) ; ?>
            </a>
        </div>
        <div class="media-body">
<!--            <h4 class="media-heading"><a-->
<!--                    href="--><?php //the_permalink(); ?><!--">--><?php //the_title(); ?><!--</a></h4>-->

            <p class="short-detail">
                <?php echo wp_trim_words(get_the_excerpt(), 15); ?>
            </p>
        </div>
    </div>
    <?php endforeach;
    wp_reset_postdata(); ?>
</div>