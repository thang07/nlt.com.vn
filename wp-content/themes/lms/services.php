<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 4/8/2015
 * Time: 3:56 PM
 */?>

<div class="servies">
    <ul class="list-item">
        <h3 class="text-uppercase"><?php echo get_cat_name( 3 ) ?></h3>

        <?php
        $args = array('category_name' => 'dich-vu','numberposts' => '8');
        $lastposts = get_posts($args);
        foreach ($lastposts as $post) :
            setup_postdata($post); ?>
            <li><i class="glyphicon glyphicon-chevron-right"></i>  &nbsp; <a href="<?php echo the_permalink()?>">  <?php the_title()?></a></li>
        <?php endforeach;
        wp_reset_postdata(); ?>
    </ul>
</div>