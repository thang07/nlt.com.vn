<link rel="stylesheet" type="text/css"
      href="<?php echo get_template_directory_uri(); ?>/carouselengine/initcarousel-1.css">

<div class="partners">

    <div style="margin:0px auto;">    <!-- Insert to your webpage where you want to display the carousel -->

        <div id="amazingcarousel-container-1">
            <div id="amazingcarousel-1"
                 style="display:block;position:relative;width:100%;max-width:990px;margin:0px auto 0px;">
                <div class="amazingcarousel-list-container">
                    <ul class="amazingcarousel-list">
                        <?php
                        $args = array('category_name' => 'khach-hang', 'order' => 'ASC', 'numberposts' => '10');
                        $lastposts = get_posts($args);
                        foreach ($lastposts as $post) :
                            setup_postdata($post); ?>
                            <li class="amazingcarousel-item">
                                <div class="amazingcarousel-item-container">
                                    <div class="amazingcarousel-image">

                                        <a href="<?php if(get_field('url-links')){echo get_field('url-links');}?>" data-group="amazingcarousel-1"
                                           class="html5lightbox" title="<?php echo the_title() ?>" target="_blank">
                                            <?php echo the_post_thumbnail('post-thumbnail', array('class' => "media-object img-responsive")); ?>

                                        </a>

                                    </div>
                                </div>
                            </li>
                        <?php endforeach;
                        wp_reset_postdata(); ?>
                    </ul
                    <div class="amazingcarousel-prev"></div>
                    <div class="amazingcarousel-next"></div>
                </div>

            </div>
        </div>
</div>
<!-- End of body section HTML codes -->
