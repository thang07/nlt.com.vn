<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lmsdb');

/** MySQL database username */
define('DB_USER', 'lms');

/** MySQL database password */
define('DB_PASSWORD', 'VAjQr7anM9Z3nEL7');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4pQqSU,/Z~meR<S?0f%^cn@96q&e:S!2u#Ah$6w0OUV@nuT*<OWFb/ZktV)6eVEV');
define('SECURE_AUTH_KEY',  'EJC{W-+h*,Mfx#$>Kq3K6_+-pxX9P:c~r#S+nYw$U`O`)@B7r 7[Jww,M&hR3rW=');
define('LOGGED_IN_KEY',    '-#iBt;S6OLq!@g3tkmVe$Zja!u`>.$%sU_-71-Zw8~<U`B]GLHe|O8=*g%ix1p*v');
define('NONCE_KEY',        '/3mI9hd{J4FpJKg-Nh@Jy#G4a,L+I8@n0.+ua+yp;~ZX|-q.b|iy0_[r*NbIs<@x');
define('AUTH_SALT',        ',)ihIHd[^KX{~} y9ah$ZD-:wc0WC=Jkc w4*b!y;mPj!Dete15-s9.x!$,7a-B}');
define('SECURE_AUTH_SALT', '$o+4;8&-N9fI((ExyM=)$]rH1l.Z,Ml|h^T:iSaKas6F-|M$Xt^j[{7H0PZZ4pw`');
define('LOGGED_IN_SALT',   ':-)gX$~alQaHdQLR!XWXS-+~Ru<jh[Uww97-PM[r(T+QxA.HWYEV)x;2)8.~E(<?');
define('NONCE_SALT',       '?-~vIkiFX9Z;0rzPdHXvJ@vpKio3{.?-Xx#ihitL-e+j,]J<?Q+Z8_V%h9Rh7[]>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'lms_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);
define( 'WP_AUTO_UPDATE_CORE', false );
define ('WPLANG', 'vi');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
